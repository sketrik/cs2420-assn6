import java.util.Scanner;

public class Driver {
	public static final int SIZE = 20;

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Number of times to average: ");
		int x = scanner.nextInt();

		float sum = 0;
		for (int i = 0; i < x; i++) {
			sum += (runSet() + 0.0)/400;
		}

		System.out.printf("Average is %f%%", sum*100/x);

	}

	public static int runSet() {
		Set set = new Set(SIZE * (SIZE + 2));

		//union top and bottom rows
		for (int i = 0; i < SIZE; i++) {
			set.union(0, i);
			set.union(set.length() - 1, set.length() - (i + 1));
		}

		boolean[][] used = new boolean[SIZE][SIZE];
		for (int i = 0; true; i++) {
			int x = (int) (Math.random() * SIZE);
			int y = (int) (Math.random() * SIZE);
			if (used[y][x]) {
				i--;
				continue;
			}
			used[y][x] = true;
			int setItem = SIZE + y * SIZE + x;
			if (y == 0) {
				set.union(setItem, 0);
			}
			if (y == SIZE - 1) {
				set.union(setItem, set.length() - 1);

			}
			//union right
			if (x + 1 < SIZE && used[y][x + 1]) {
				set.union(setItem, setItem + 1);
			}
			//union down
			if (y + 1 < SIZE && used[y + 1][x]) {
				set.union(setItem, setItem + SIZE);
			}
			//union left
			if (x > 0 && used[y][x - 1]) {
				set.union(setItem, setItem - 1);
			}
			//union up
			if (y > 0 && used[y - 1][x]) {
				set.union(setItem, setItem - SIZE);
			}
			if ((i + 1) % 50 == 0) {
				System.out.printf("Percolation at %d: \n", i + 1);
				print(used);
			}
			if (set.isSameGroup(0, set.length() - 1)) {
				System.out.printf("SOLUTION FOUND AT %d (last square at %d, %d [bottom left is origin])\n", i + 1, x+1, SIZE-y);
				print(used);
				return i + 1;
			}
		}
	}

	public static void print(boolean[][] array) {
		StringBuilder sb = new StringBuilder();
		char[] sym = {'\u25A0', '\u25A2'};  //black square, white square
		for (int i = 0; i < SIZE; i++) {
			for (int j = 0; j < SIZE; j++) {
				sb.append(array[i][j] ? sym[0] : sym[1]);
			}
			sb.append('\n');
		}
		System.out.println(sb.toString());
	}
}
