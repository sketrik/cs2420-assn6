public class Set {
	public static int SIZE;
	private int[][] list;

	public Set(int size) {
		SIZE = size;
		list = new int[SIZE][2];
		for (int i = 0; i < list.length; i++) {
			int[] r = {i, 1};
			list[i] = r;
		}
	}

	public int length() {
		return list.length;
	}

	public boolean isSameGroup(int x, int y) {
		return find(x) == find(y);
	}

	public boolean union(int x, int y) {
//		System.out.printf("unioned %d and %d\n", x, y);
		return union(x, y, 0) != -1;
	}

	private int union(int x, int y, int z) {
		int parentX = list[x][0];
		int parentY = list[y][0];
		int root;
		if (parentX != x && parentY != y) {
			root = union(parentX, parentY, z);
		} else if (parentX != x) {
			root = union(parentX, y, z);
		} else if (parentY != y) {
			root = union(x, parentY, z);
		} else {
			if (parentX == parentY) {
				return -1;
			}
			int size = list[y][1] + list[x][1];
			if (list[x][1] >= list[y][1]) {
				root = x;
			} else {
				root = y;
			}
			list[root][1] = size;
		}
		if (root == -1) {
			return -1;
		}
		if (x != root) {
			list[x][0] = root;
			list[x][1] = 1;
		}
		if (y != root) {
			list[y][0] = root;
			list[y][1] = 1;
		}
		return root;
	}

	public int find(int n) {
		while (n != list[n][0]) {
			n = list[n][0];
		}
		return n;
	}

	public void print() {
		System.out.println("index | parent | size");
		for (int i = 0; i < list.length; i++) {
			System.out.printf("%2d %2d %2d\n", i, list[i][0], list[i][1]);
		}
	}

}
