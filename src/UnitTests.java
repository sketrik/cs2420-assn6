import org.junit.Assert;
import org.junit.Test;

public class UnitTests {

	@Test
	public void testUnion() {
		Set set = new Set(30);
		Assert.assertTrue("Find thinks two groups are same", !set.isSameGroup(1, 2));
		set.union(1, 2);
		System.out.println("Unioned 1, 2");
		set.print();
		Assert.assertTrue("Find thinks same groups are different", set.isSameGroup(1, 2));
		Assert.assertTrue("Find thinks two groups are same", !set.isSameGroup(0, 2));
		set.union(3, 2);
		System.out.println("Unioned 3, 2");
		set.print();
		Assert.assertTrue("Find thinks same groups are different", set.isSameGroup(3, 2));
		Assert.assertTrue("Find thinks same groups are different", set.isSameGroup(1, 2));
		Assert.assertTrue("Find thinks two groups are same", !set.isSameGroup(0, 2));
		set.union(4, 5);
		System.out.println("Unioned 4, 5");
		set.print();
		set.union(7, 6);
		System.out.println("Unioned 7, 6");
		set.print();
		set.union(4, 7);
		System.out.println("Unioned 4, 7");
		set.print();
		set.union(3, 7);
		System.out.println("Unioned 3, 7");
		set.print();
		Assert.assertTrue("Find thinks same groups are different", set.isSameGroup(4, 7));
		Assert.assertTrue("Find thinks same groups are different", set.isSameGroup(5, 7));
		Assert.assertTrue("Find thinks same groups are different", set.isSameGroup(2, 7));
		Assert.assertTrue("Find thinks same groups are different", set.isSameGroup(3, 4));
		Assert.assertTrue("Find thinks two groups are same", !set.isSameGroup(0, 7));
		set.print();
	}
}
